var BaseRequest = require('request');
var oauth = require('oauth-sign');
var _ = require('lodash');
var fs = require('fs');

var request = BaseRequest.defaults({
	proxy:'http://127.0.0.1:8888'
});

/**
 * [PhotobucketAPI description]
 * @param {[type]} consumer_key    [description]
 * @param {[type]} consumer_secret [description]
 * @param {[type]} token           [description]
 * @param {[type]} token_secret    [description]
 * @param {[type]} subdomain       [description]
 */
function PhotobucketAPI(consumer_key,  consumer_secret,  token, token_secret, subdomain){
	this.baseUrl = 'http://api.photobucket.com';	
	this.consumer_key = consumer_key;
	this.consumer_secret = consumer_secret;
	this.token = token;
	this.token_secret = token_secret;
	this.subdomain = subdomain;
}
/**
 * [baseParams description]
 * @return {[type]} [description]
 */
PhotobucketAPI.prototype.baseParams = function() {
	return {
		oauth_consumer_key: this.consumer_key,
		oauth_nonce:'hello',
		oauth_signature_method:'HMAC-SHA1',	
		oauth_timestamp: Math.floor( Date.now() / 1000 ).toString(),
		oauth_token: this.token,
		oauth_version:'1.0'
	};
};

/**
 * [makeOAuthParams description]
 * @param  {[type]} httpMethod      [description]
 * @param  {[type]} url             [description]
 * @param  {[type]} params          [description]
 * @param  {[type]} consumer_secret [description]
 * @param  {[type]} token_secret    [description]
 * @return {[type]}                 [description]
 */
PhotobucketAPI.prototype.makeOAuthParams = function(httpMethod, url, params, consumer_secret, token_secret){
	var sign = oauth.sign('HMAC-SHA1', httpMethod, url, params, this.consumer_secret, this.token_secret);
	var oAuthParams = _.extend({ oauth_signature: sign }, params);
	return oAuthParams;
};

PhotobucketAPI.prototype.concatParams = function(oa, sep, wrap) {
	wrap = wrap || '';
	sep = sep || ',';

  	var params = Object.keys(oa).filter(function (i) {
	   return i !== 'realm' && i !== 'oauth_signature'
	}).sort();

	if (oa.realm) {
	   params.splice(0, 1, 'realm');
	}
	params.push('oauth_signature');

	return params.map(function (i) {
	    return i + '=' + wrap + oauth.rfc3986(oa[i]) + wrap;
	}).join(sep);
};

/**
 * [ping description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
PhotobucketAPI.prototype.ping = function(callback) {
	var params = this.baseParams();

	request.get({
		url: this.baseUrl+'/ping',	
		headers: {
	    	'Authorization': 'OAuth '+ this.concatParams(this.makeOAuthParams('GET', this.baseUrl+'/ping', params),',', '"')
	    }
	}, callback);
};

/**
 * [album description]
 * @param  {[type]}   id       [description]
 * @param  {Function} callback [description]
 * @return {[type]}            [description]
 */
PhotobucketAPI.prototype.album = function(id, callback) {	
	var relUrl = '/album/'+id;
	var params = this.baseParams();
	params.format='json';

	request.get({
		url:  this.subdomain + relUrl + '?format=json',
		headers: {
	    	'Authorization': 'OAuth '+ this.concatParams(this.makeOAuthParams('GET', this.baseUrl+relUrl, params),',', '"')
	    }
	}, callback);
};

/**
 * upload file to server
 * @param  {String}   id       filder id
 * @param  {String}   filename file path
 * @param  {Object}   params   Extra parameters, type is requred
 * @param  {Function} callback function
 */
PhotobucketAPI.prototype.upload = function(id, filename, params, callback) {
    var relUrl = '/album/' + id + '/upload';
    
    var formData = _.extend({ uploadfile: null }, params);
    formData.uploadfile = fs.createReadStream(filename);
    
    params = _.extend(params, this.baseParams());
    params.format = 'json';
    
    request.post({
        url: this.subdomain + relUrl + '?format=json',
        headers: {
            'Authorization': 'OAuth ' + this.concatParams(this.makeOAuthParams('POST', this.baseUrl + relUrl, params), ',', '"')
        },
        formData: formData,
    }, callback);
};

module.exports = PhotobucketAPI;